﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using RestaurantApp.Calory_Calculator;
using RestaurantApp.Menu;

namespace RestaurantApp
{
    /// <summary>
    /// Interaction logic for OrderWindow.xaml
    /// </summary>
    public partial class OrderWindow : Window
    {
        private SqlFind sqlFind;
        private OrderWindowToolsManager toolsManager;
        OrderViewModel viewModel = new OrderViewModel();
        private string queryForPizza = "SELECT * FROM Product WHERE product_type LIKE 'Pizza'";
        private string queryForDinner = "SELECT * FROM Product WHERE product_type LIKE 'Obiad'";
        private string queryForDrinks = "SELECT * FROM Product WHERE product_type LIKE 'Napoj'";
        public List<Product> ProductsInMenu { get; set; }
        public decimal TotalBruttoPrice { get; set; }
        public decimal TotalNettoPrice { get; set; }
        public string NettoPriceString { get; set; }
        public string BruttoPriceString { get; set; }

        public OrderWindow()
        {
            sqlFind=new SqlFind();
            toolsManager=new OrderWindowToolsManager();
            ProductsInMenu = new List<Product>();

            this.BruttoPriceString = this.TotalBruttoPrice + " zł";
            this.NettoPriceString = this.TotalBruttoPrice + " zł";
            this.DataContext = viewModel;

            InitializeComponent();
            toolsManager.fillComboBox();
        }


        private void changeListBoxContent(object sender, EventArgs e)
        {
            ProductsInMenu.Clear();
            MenuListBox.ItemsSource = null;

            switch (productTypesComboBox.SelectedIndex)
            {
                case 0:
                    ProductsInMenu=sqlFind.GetProducts(queryForPizza);
                    break;
                case 1:
                    ProductsInMenu=sqlFind.GetProducts(queryForDinner);
                    break;
                case 2:
                    ProductsInMenu=sqlFind.GetProducts(queryForDrinks);
                    break;
                default: break;
            }

            MenuListBox.ItemsSource = ProductsInMenu;
        }


        private void OrderProductButtonClick(object sender, RoutedEventArgs e)
        {
            Product selectedMenuItem = (Product)MenuListBox.SelectedItem;
            ElementsOfOrder.Items.Clear();

            if (productTypesComboBox.SelectedIndex == -1 || selectedMenuItem == null)
            {
                MessageBox.Show("wybierz jakiś produkt");
                return;
            }

            string selectedProductAsString = selectedMenuItem.Name;
            decimal selectedBruttoPrice = sqlFind.FindBruttoPrice(selectedProductAsString);

            if (selectedBruttoPrice != -1)     //-1 tylko wtedy gdy nie znaleziono w bbazie danych
            {
                this.TotalBruttoPrice += selectedBruttoPrice;
                this.TotalBruttoPrice = Math.Round(TotalBruttoPrice, 2, MidpointRounding.AwayFromZero);
                this.BruttoPriceString = TotalBruttoPrice + " zł";
                viewModel.LabelBruttoPrice = BruttoPriceString;
            }

            decimal selectedNettoPrice = sqlFind.FindNettoPrice(selectedProductAsString);

            if (selectedNettoPrice != -1)       //-1 tylko wtedy gdy nie znaleziono w bazie danych
            {
                this.TotalNettoPrice += selectedNettoPrice;
                this.TotalNettoPrice = Math.Round(TotalNettoPrice, 2, MidpointRounding.AwayFromZero);
                this.NettoPriceString = TotalNettoPrice + " zł";
                viewModel.LabelNettoPrice = NettoPriceString;
            }

            toolsManager.addProductToList(selectedProductAsString, selectedBruttoPrice);
            toolsManager.addElementsFromListToListBox();
        }

        

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            string bill = "";

            foreach (string productOnBill in toolsManager.ListOfProducts)
            {
                bill += productOnBill + "\n";
            }
            MessageBox.Show("Zamówiłeś:\n"+bill+"Cena netto: "+NettoPriceString+"\n"+"Cena brutto: "+BruttoPriceString+"\nProsimy czekać na zamówienie");
        }

        private void openCalculator_Click(object sender, RoutedEventArgs e)
        {
            CalculatorWindow calculatorWindow=new CalculatorWindow();
            calculatorWindow.Show();
        }
    }
}
