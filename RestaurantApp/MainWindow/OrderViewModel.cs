﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RestaurantApp
{
    class OrderViewModel : INotifyPropertyChanged
    {
        private string _labelBruttoPrice;
        private string _labelNettoPrice;
        public string LabelBruttoPrice
        {
            get { return _labelBruttoPrice; }
            set
            {
                _labelBruttoPrice = value;
                NotifyPropertyChanged("LabelBruttoPrice");
            }
        }

        public string LabelNettoPrice
        {
            get { return _labelNettoPrice; }
            set
            {
                _labelNettoPrice = value;
                NotifyPropertyChanged("LabelNettoPrice");
            }
        }

        public OrderViewModel()
        {

        }

        public event PropertyChangedEventHandler PropertyChanged;

        private void NotifyPropertyChanged(String propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }
}
