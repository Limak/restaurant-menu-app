﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RestaurantApp.Menu
{
    ///<summary>
    /// klasa zawierajaca podstawowe wlasciwosci kazdego progduktu
    /// </summary>
    public class Product
    {
        public string Name { get; set; }
        public decimal BruttoPrice { get; set; }
        public string BruttoPriceString { get; set; }
        public string Description { get; set; }
        public string Calories { get; set; }
        public int CaloriesInt { get; set; }


        public Product()
        {
        }

        public Product(string name, string description, int calories)
        {
            this.Name = name;
            this.Description = description;
            this.Calories = calories.ToString() + " kcal";
            this.CaloriesInt = calories;
        }

        public Product(string name, decimal bruttoPrize)
        {
            this.Name = name;
            this.BruttoPrice = bruttoPrize;
        }

        public Product(string name, string bruttoPrice)
        {
            this.BruttoPriceString = bruttoPrice;
            this.Name = name;
        }


    }
}
