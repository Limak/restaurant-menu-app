﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RestaurantApp.MainWindow.Calory_Calculator
{
    class CalculatorViewModel : INotifyPropertyChanged
    {
        private string _labelSummaryCalories;

        public string LabelSummaryCalories
        {
            get { return _labelSummaryCalories; }
            set
            {
                _labelSummaryCalories = value;
                NotifyPropertyChanged("LabelSummaryCalories");
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        private void NotifyPropertyChanged(String propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }
}
