﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using RestaurantApp.MainWindow.Calory_Calculator;
using RestaurantApp.Menu;

namespace RestaurantApp.Calory_Calculator
{
    /// <summary>
    /// Interaction logic for CalculatorWindow.xaml
    /// </summary>
    public partial class CalculatorWindow : Window
    {
        private SqlFind sqlFind;
        private CalculatorWindowToolsManager calculatorTools;
        private CalculatorViewModel viewModel;
        private string SummaryCaloriesString;
        public int SummaryCalories { get; set; }

        public CalculatorWindow()
        {
            sqlFind = new SqlFind();
            calculatorTools = new CalculatorWindowToolsManager(sqlFind);
            viewModel = new CalculatorViewModel();

            InitializeComponent();
            MenuListView.ItemsSource = null;
            this.Loaded += CalculatorWindow_Loaded;
            this.DataContext = viewModel;
        }

        private void CalculatorWindow_Loaded(object sender, RoutedEventArgs e)
        {
            calculatorTools.fillMenuListView(this.MenuListView);
        }

        private void AddToCalculateButton_Click(object sender, RoutedEventArgs e)
        {
            Product selectedMenuItem = (Product)MenuListView.SelectedItem;
            CalculatorListView.ItemsSource = null;

            if (selectedMenuItem == null)
            {
                MessageBox.Show("Wybierz jakiś produkt");
                return;
            }

            calculatorTools.productsToCalculate.Add(selectedMenuItem);
            CalculatorListView.ItemsSource = calculatorTools.productsToCalculate;

            this.SummaryCalories += selectedMenuItem.CaloriesInt;
            this.SummaryCaloriesString = this.SummaryCalories.ToString() + " kcal";
            this.viewModel.LabelSummaryCalories = this.SummaryCaloriesString;
        }
    }
}
