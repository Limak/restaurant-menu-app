﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using RestaurantApp.Calory_Calculator;
using RestaurantApp.Menu;

namespace RestaurantApp.MainWindow.Calory_Calculator
{
    class CalculatorWindowToolsManager
    {
        public List<Product> productsFromDatabaseList;
        public List<Product> productsToCalculate;
        private SqlFind sqlFind;

        public CalculatorWindowToolsManager(SqlFind sqlFind)
        {
            productsFromDatabaseList = new List<Product>();
            productsToCalculate=new List<Product>();
            
            this.sqlFind = sqlFind;

        }

        public void fillMenuListView(ListView menuListView)
        {
            menuListView.Items.Clear();
            productsFromDatabaseList=sqlFind.FindAllProducts();
            menuListView.ItemsSource = productsFromDatabaseList;
        }

        public void fillCalculatorListView(ListView calculatorListView)
        {
            calculatorListView.Items.Clear();
            calculatorListView.ItemsSource = productsToCalculate;
        }

    }
}
