﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using RestaurantApp.Menu;

namespace RestaurantApp
{
    /// <summary>
    /// logika do narzedzi w oknie gdzie wybiera sieprodukty na zamowienie
    /// </summary>
    public class OrderWindowToolsManager
    {
        private OrderWindow mainWindow = (OrderWindow) Application.Current.MainWindow;
        public List<string> ListOfProducts { get; set; }

        public OrderWindowToolsManager()
        {
            ListOfProducts = new List<string>();
        }

        public void fillComboBox()
        {
            string[] types = { "Pizza", "Obiady", "Napoje" };

            foreach (string type in types)
            {
                mainWindow.productTypesComboBox.Items.Add(type);
            }
        }

        public void addProductToList(string selectedProduct, decimal bruttoPrice)
        {
            string productToDisplayInWindow = selectedProduct.ToString() + "   " + bruttoPrice.ToString() + "zł";
            ListOfProducts.Add(productToDisplayInWindow);
        }

        public void addElementsFromListToListBox()
        {
            foreach (string product in ListOfProducts)
            {
                mainWindow.ElementsOfOrder.Items.Add(product);
            }
        }
    }
}
