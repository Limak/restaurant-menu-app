﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using RestaurantApp.Menu;

namespace RestaurantApp
{
    public class SqlFind
    {
        private readonly string dbString =
            @"Data Source=(LocalDB)\MSSQLLocalDB;
            AttachDbFilename=C:\Users\Kamil\Documents\RestaurantMenu.mdf;
            Integrated Security=False;
            Connect Timeout=30;
            User Instance=False";

        private OrderWindowToolsManager orderWindowToolsManager;

        public SqlFind()
        {
            orderWindowToolsManager = new OrderWindowToolsManager();
        }

        public decimal FindBruttoPrice(string selectedProduct)
        {
            decimal returnedBruttoPrice = -1;
            using (var connection = new SqlConnection(this.dbString))
            {
                connection.Open();

                string query = "select * from Product where Name like '" + selectedProduct + "'";
                var productsNamesFromSqlCommand = new SqlCommand(query, connection);
                var productsNamesReader = productsNamesFromSqlCommand.ExecuteReader();

                while (productsNamesReader.Read())
                {
                    string productName = productsNamesReader.GetString(1);

                    if (productName.Equals(selectedProduct))
                    {
                        decimal foundProductBruttoPrice = productsNamesReader.GetDecimal(4);
                        foundProductBruttoPrice = Math.Round(foundProductBruttoPrice, 2);

                        returnedBruttoPrice = foundProductBruttoPrice;
                        return returnedBruttoPrice;
                    }
                }
            }
            return returnedBruttoPrice;
        }

        public decimal FindNettoPrice(string selectedProduct)
        {
            decimal returnedNettoPrice = -1;
            using (var connection = new SqlConnection(this.dbString))
            {
                connection.Open();

                string query = "select * from Product where Name like '" + selectedProduct + "'";
                var productsNamesFromSqlCommand = new SqlCommand(query, connection);
                var productsNamesReader = productsNamesFromSqlCommand.ExecuteReader();

                while (productsNamesReader.Read())
                {
                    string productName = productsNamesReader.GetString(1);

                    if (productName.Equals(selectedProduct))
                    {
                        decimal foundProductNettoPrice = productsNamesReader.GetDecimal(3);
                        foundProductNettoPrice = Math.Round(foundProductNettoPrice, 2);

                        returnedNettoPrice = foundProductNettoPrice;
                        return returnedNettoPrice;
                    }
                }

            }
            return returnedNettoPrice;
        }

        public List<Product> GetProducts(string query)
        {
            var returnedProductsInMenu = new List<Product>();

            using (var connection = new SqlConnection(this.dbString))
            {
                connection.Open();

                var productsNamesFromSqlCommand = new SqlCommand(query, connection);
                var productsNamesReader = productsNamesFromSqlCommand.ExecuteReader();

                while (productsNamesReader.Read())
                {

                    string productName = productsNamesReader.GetString(1);
                    decimal productPriceBrutto = productsNamesReader.GetDecimal(4);
                    string productPrice = productPriceBrutto.ToString() + " zł";

                    Product product = new Product()
                    {
                        Name = productName,
                        BruttoPriceString = productPrice,
                        BruttoPrice = productPriceBrutto
                    };
                    returnedProductsInMenu.Add(product);
                }

            }
            return returnedProductsInMenu;
        }

        public List<Product> FindAllProducts()
        {
            var searchedProducts = new List<Product>();
            using (var connection = new SqlConnection(this.dbString))
            {
                string query = "SELECT * FROM Product ";
                connection.Open();

                var productsFromSql = new SqlCommand(query, connection);
                var productsReader = productsFromSql.ExecuteReader();

                while (productsReader.Read())
                {
                    string productName = productsReader.GetString(1);
                    string productDescription = productsReader.GetString(2);
                    int productCalories = productsReader.GetInt32(5);

                    Product product=new Product(productName, productDescription,productCalories);

                    //var product = new Product()
                    //{
                    //    Name = productName,
                    //    Description = productDescription,
                    //    Calories = productCalories.ToString()
                    //};

                    searchedProducts.Add(product);
                }
            }
            return searchedProducts;
        }
    }
}
