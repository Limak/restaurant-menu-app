# Ostateczny opis #
https://youtu.be/F5d3fwxHdk4

Simple Application to made order in restaurant. I'm going to implement also simple guide for people who don't know what to eat on each
type of meal. This application will also callculate calories for you.

### What is this repository for? ###

Project made for C# Academy course. One of the requirements getting a Cerfificate

### What I use in my project ###

* Using WPF
* Using Object Oriented Programming
* Main language: C#
* Trying to write own Unit Tests
* Maybe in future multiplatform appication

### What has been done ###
* WPF
* Object Oriented Programming
* MSSQL
* Binding
* Type of meals
* Calculating calories
* Calculating bills

### Who do I talk to? ###

* contact: kamil.mastalerz7@wp.pl